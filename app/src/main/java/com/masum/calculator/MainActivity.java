package com.masum.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public EditText Result;
    public float firstNumber;
    public float finalNumber;
    public String operation;
    private ButtonClickListener Btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Result = (EditText)findViewById(R.id.result);
        Result.setEnabled(false);
        Btn = new ButtonClickListener();
        int idsList[] = {R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9, R.id.btnZero, R.id.btnDot, R.id.btnEql, R.id.btnAdd, R.id.btnSub, R.id.btnMul, R.id.btnDiv, R.id.btnAc,R.id.btnC};

        for(int id : idsList){
            View v = findViewById(id);
            v.setOnClickListener(Btn);
        }
    }

    public void math(String  string){
        firstNumber = Float.parseFloat(Result.getText().toString());
        operation = string;
        Result.setText("0");
    }

    public void keyboard(String string){
        String current = Result.getText().toString();

        if(current.equals("0")) {
            current = " ";
        }
            current += string;
            Result.setText(current);

    }

    private void result(){
        float finalNumber = Float.parseFloat(Result.getText().toString());
        float result = 0;

        if(operation.equals("+")){
            result = firstNumber + finalNumber;
        }
        if (operation.equals("-")){
            result = firstNumber - finalNumber;
        }
        if(operation.equals("*")){
            result = firstNumber * finalNumber;
        }
        if(operation.equals("/")){
            result = firstNumber / finalNumber;
        }

        Result.setText(String.valueOf(result));
    }

    private  class ButtonClickListener implements View.OnClickListener{
        public void onClick(View v){
            switch (v.getId()){
                case R.id.btnAc:
                    Result.setText("0");
                    operation = " ";
                    break;
                case R.id.btnAdd:
                    math("+");
                    break;
                case R.id.btnSub:
                    math("-");
                    break;
                case R.id.btnMul:
                    math("*");
                    break;
                case R.id.btnDiv:
                    math("/");
                    break;
                case R.id.btnEql:
                    result();
                    break;
                default:
                    String number = ((Button)v).getText().toString();
                    keyboard(number);
                    break;
            }
        }
    }
}
